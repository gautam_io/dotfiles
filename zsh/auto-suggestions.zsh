# Config for ZSH auto suggestions
# https://github.com/zsh-users/zsh-autosuggestions

## Load 
source $HOMEBREW_PREFIX/share/zsh-autosuggestions/zsh-autosuggestions.zsh

## Set suggestion color
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=2'

## Bind ctrl + space to accept the current suggestion
bindkey '^ ' autosuggest-accept
