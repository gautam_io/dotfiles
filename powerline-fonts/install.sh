#!/bin/sh

echo "Installing powerline fonts"
# Install powerline fonts
git clone https://github.com/powerline/fonts.git --depth=1

cd fonts
sudo ./install.sh

# clean-up a bit
cd ..
rm -rf fonts
